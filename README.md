How to run
-----------------------
1 check out code to a directory (e.g. foo)

2 Start mongod

>cd foo

>mkdir data

>mongod --dbpath ./data

3 Start the application

>cd foo

>mvn spring-boot:run

4 Use postman or any http client to send a request to the REST service.

4.1 Login a user

URL:

POST, http://localhost:8080/login

REQUEST HEADER: 

{ 

    "Content-Type": application/json" 
    
}

REQUEST BODY:

{

    "name": "admin1",
    "password": "password"
    
}

NOTE:

  4 users are available:

{   
   "name": "admin1",   
   "password": "password",   
   "role": "admin"  
}

{    
   "name": "admin2",   
   "password": "password",  
   "role": "admin"  
}

{   
   "name": "bob",   
   "password": "password",  
   "role": "normal"   
}

{   
   "name": "ben",  
   "password": "password",  
   "role": "normal"  
}

RESPONSE:

{

    "status": "success",
    "statusText": "",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4xIn0.jXM9oKcNWklRRgORjSVlTNk0FDV36Ntm9dqWsBnCDps"

}

4.2 Register a new customer

URL:

POST, http://localhost:8080/register

REQUEST HEADER:

{ 

    "Content-Type": application/json",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4xIn0.jXM9oKcNWklRRgORjSVlTNk0FDV36Ntm9dqWsBnCDps"

}

REQUEST BODY:

{

    "firstName": "Foo",
    "lastName": "Bar",
    "password": "pwd",
    "phoneNumber": "0846512438",
    "address": "123 main st, BKK",
    "salary": "60000"
    
}

RESPONSE:

{

    "status": "success",
    "statusText": "Customer created",
    "customer": {
        "id": "5a89bb0b7c55b8038d6610d5",
        "firstName": "Foo",
        "lastName": "Bar",
        "password": "$2a$10$ZyNG/5.aIz8.A1P8O03B.uPm8Hy9WMx8ump1K1kLdVLAwa6dTrwA.",
        "phoneNumber": "0846512438",
        "address": "123 main st, BKK",
        "salary": 60000,
        "memberType": 1,
        "referenceCode": {
            "data": "201802192438"
        },
        "fullName": "Foo Bar",
        "memberTypeString": "Platinum"
    }
    
}

4.3 Search for a customer using customer ID

URL:

GET, http://localhost:8080/register/search?id=5a89bb0b7c55b8038d6610d5

REQUEST HEADER:

{ 

    "Content-Type": application/json",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4xIn0.jXM9oKcNWklRRgORjSVlTNk0FDV36Ntm9dqWsBnCDps"

}

REQUEST BODY:

  -

RESPONSE:

{

    "status": "success",
    "statusText": "",
    "customer": {
        "id": "5a89bb0b7c55b8038d6610d5",
        "firstName": "Foo",
        "lastName": "Bar",
        "password": "$2a$10$ZyNG/5.aIz8.A1P8O03B.uPm8Hy9WMx8ump1K1kLdVLAwa6dTrwA.",
        "phoneNumber": "0846512438",
        "address": "123 main st, BKK",
        "salary": 60000,
        "memberType": 1,
        "referenceCode": {
            "data": "201802192438"
        },
        "fullName": "Foo Bar",
        "memberTypeString": "Platinum"
    }
    
}

4.4 Search for a customer using customer's first and last name

URL: 

GET, http://localhost:8080/register/search?firstName=Foo&lastName=Bar

REQUEST HEADER:

{ 

    "Content-Type": application/json",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4xIn0.jXM9oKcNWklRRgORjSVlTNk0FDV36Ntm9dqWsBnCDps"

}

REQUEST BODY:

  -

RESPONSE:

{

    "status": "success",
    "statusText": "",
    "customer": {
        "id": "5a89bb0b7c55b8038d6610d5",
        "firstName": "Foo",
        "lastName": "Bar",
        "password": "$2a$10$ZyNG/5.aIz8.A1P8O03B.uPm8Hy9WMx8ump1K1kLdVLAwa6dTrwA.",
        "phoneNumber": "0846512438",
        "address": "123 main st, BKK",
        "salary": 60000,
        "memberType": 1,
        "referenceCode": {
            "data": "201802192438"
        },
        "fullName": "Foo Bar",
        "memberTypeString": "Platinum"
    }
    
}



REST services
-----------------------
1 "/login"
  For logging in a user

2 "/register"
  For creating a new customer

3 "/register?<param>"
  For looking up an existing customer

NOTE:
 4 users are hardcoded as follows:
 
 { 
   "name": "admin1",
   
   "password": "password"
 }

 { 
   "name": "admin2",
   
   "password": "password"
 }

 { 
   "name": "bob",
   
   "password": "password"
 }

 { 
   "name": "ben",
   
   "password": "password"
 }

"/login" Service
-----------------------
- Before you can perform a customer registration or customer lookup, you have to log in with an "admin-privileged" user.
- If a login user has "admin" privilege, he or she will receive a JWT token in the login response. The token is needed for accessing "/register" service.

- To log in, POST with user login data such as

{ 

  "name"    : "admin2",
  
  "password": "password"
  
}

- A SUCCESSFUL response includes a JWT token for subsequent requests: 

{

    "status": "success",
    
    "statusText": "",
    
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4yIn0.Syq95o2FcK16VrcGJqsJX3zuV1msx2ClZBdn6VAn-JU"
}

- UNSUCCESSFUL responses:

{

    "status": "fail",
    
    "statusText": "Invalid user",
    
    "token": ""
}

{

    "status": "fail",
    
    "statusText": "Invalid password",
    
    "token": ""
    
    
}

{

    "status": "fail",
    
    "statusText": "No user name",
    
    "token": ""
    
}

{

    "status": "fail",
    
    "statusText": "No password",
    
    "token": ""
    
}



"/register" service
-----------------------
- A service for registering a new customer or looking up an existing customer.
- To access this service, a JWT token is required in the header. A JWT token is obtained from a successful login response of an "admin-privileged" user. You can include a token in the header as follows:
    { "token":  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlIjoiYWRtaW4iLCJuYW1lIjoiYWRtaW4yIn0.Syq95o2FcK16VrcGJqsJX3zuV1msx2ClZBdn6VAn-JU" }


- To create a new customer, POST to the service as follows: 

{

    "firstName"  : "Bob",
    
    "lastName"   : "Brown",
    
    "password"   : "password",
    
    "phoneNumber": "0894551234",
    
    "address"    : "123 main st, BKK",
    
    "salary"     : "60000"
    
}

- A SUCCESSFUL response looks like:

{

    "status": "success",
    
    "statusText": "Customer created",
    
    "customer": {
    
        "id": "5a861c160db93f2415e6217f",
        
        "firstName": "Bob",
        
        "lastName": "Brown",
        
        "password": "$2a$10$B4GiFyhvpJPvhh3ugfOF9ubfamF5KTcc2rTHhVw3f2cdancOFK5wG",
        
        "phoneNumber": "0894551234",
        
        "address": "123 main st, BKK",
        
        "salary": 60000,
        
        "memberType": 1,
        
        "referenceCode": {
        
            "data": "201802161234"
            
        },
        
        "fullName": "Bob Brown",
        
        "memberTypeString": "Platinum"
        
    }
    
}

- An UNSUCCESSFUL response may look like:

{

    "status": "fail",
    
    "statusText": "Customer already exists: Bob Brown, 5a861c160db93f2415e6217f",
    
    "customer": null
    
}


- To query for an existing customer, use either "id" or "firstName&lastName":

    "/register/search?id=5a861c160db93f2415e6217f"
    
    "/register/search?firstName=Bob&lastName=Brown"

- A SUCCESSFUL response:

{

    "status": "success",
    
    "statusText": "",
    
    "customer": {
    
        "id": "5a861c160db93f2415e6217f",
        
        "firstName": "Bob",
        
        "lastName": "Brown",
        
        "password": "$2a$10$B4GiFyhvpJPvhh3ugfOF9ubfamF5KTcc2rTHhVw3f2cdancOFK5wG",
        
        "phoneNumber": "0894551234",
        
        "address": "123 main st, BKK",
        
        "salary": 60000,
        
        "memberType": 1,
        
        "referenceCode": {
        
            "data": "201802161234"
            
        },
        
        "fullName": "Bob Brown",
        
        "memberTypeString": "Platinum"
        
    }
    
}

- UNSUCCESSFUL responses:

{

    "timestamp": 1518738672579,
    
    "status": 400,
    
    "error": "Bad Request",
    
    "exception": "org.springframework.web.bind.ServletRequestBindingException",
    
    "message": "Missing request header 'token' for method parameter of type String",
    
    "path": "/register/search"
    
}

{

    "status": "fail",
    
    "statusText": "Bad token",
    
    "customer": null
    
}

{

    "status": "fail",
    
    "statusText": "Customer not found",
    
    "customer": null
    
}
