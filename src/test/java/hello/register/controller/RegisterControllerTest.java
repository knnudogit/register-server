package hello.customer.register;

import com.fasterxml.jackson.databind.ObjectMapper;
import hello.Application;
import hello.customer.domain.Customer;
import hello.register.service.CustomerServiceClient;
import hello.login.service.LoginServiceClient;
import hello.register.controller.RegisterController;
import hello.customer.domain.Response;
import hello.customer.domain.ReferenceCode;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.notNullValue;
import org.hamcrest.Matcher;

import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.ArgumentMatchers.any;

import java.lang.Exception;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.FieldPosition;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class RegisterControllerTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @InjectMocks
  private RegisterController registerController;
  
  @Mock
  private CustomerServiceClient customerServiceClient;

  @Mock
  private LoginServiceClient loginServiceClient;

  private MockMvc mockMvc;
  
  @Before
  public void setup() {
  	initMocks(this);
  	this.mockMvc = MockMvcBuilders.standaloneSetup(registerController).build();
  }

  @Test
  public void testReferenceCode() throws Exception {

    //Get current DATE string
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
    StringBuffer buf = new StringBuffer();
    FieldPosition fp = new FieldPosition(0);
    df.format(new Date(), buf, fp);
  
    //Phone number is MORE THAN 4 digits, successful
    final String LAST_FOUR_1    = "3241";
    final String phoneNumber1 = "084651" + LAST_FOUR_1;
    ReferenceCode refCode1 = new ReferenceCode(phoneNumber1);
    Assert.assertTrue(refCode1.getData().equals(buf + LAST_FOUR_1));

    //Phone number is EXACTLY 4 digits, successful
    final String LAST_FOUR_2  = "3241";
    final String phoneNumber2 = "" + LAST_FOUR_2;
    ReferenceCode refCode2 = new ReferenceCode(phoneNumber2);
    Assert.assertTrue(refCode2.getData().equals(buf + LAST_FOUR_2));

    //Phone number is LESS THAN 4 digits, failed
    try {
      final String LAST_FOUR_3  = "321";
      final String phoneNumber3 = "" + LAST_FOUR_3;
      ReferenceCode refCode3 = new ReferenceCode(phoneNumber3);
    } catch (Exception e) {
      Assert.assertTrue(e.getMessage().equals("Bad phone number"));
    }
  }

  @Test
  public void testCalculateMemberType() throws Exception {

    final Integer PLATINUM_SALARY_MIN = 50001;
    final Integer GOLD_SALARY_MAX = 50000;
    final Integer GOLD_SALARY_MIN = 30000;
    final Integer SILVER_SALARY_MAX = 29999;
    final Integer SILVER_SALARY_MIN = 15000;
  
    Assert.assertEquals(Customer.MEMBER_TYPE_PLATINUM, registerController.computeMemberType(PLATINUM_SALARY_MIN));
    Assert.assertEquals(Customer.MEMBER_TYPE_GOLD, registerController.computeMemberType(GOLD_SALARY_MAX));
    Assert.assertEquals(Customer.MEMBER_TYPE_GOLD, registerController.computeMemberType(GOLD_SALARY_MIN));
    Assert.assertEquals(Customer.MEMBER_TYPE_SILVER, registerController.computeMemberType(SILVER_SALARY_MAX));
    Assert.assertEquals(Customer.MEMBER_TYPE_SILVER, registerController.computeMemberType(SILVER_SALARY_MIN));
  }

  @Test
  public void shouldCreateNewCustomer() throws Exception {

    final String TOKEN = "12346uwfbdasBchSDBchjsDc.zjxczkjxczkjxcbjzxc.aiwhsjznckjznckjnzxcjk";
    final String PASSWORD = "pwd";
    final Customer customer = new Customer();
    final Integer PLATINUM_SALARY = 50001;
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setPassword(PASSWORD);
    customer.setSalary(PLATINUM_SALARY);
    customer.setPhoneNumber("0846565123");
    customer.setAddress("Foo Building, Bangkok");
  
    //token verified successfully
    hello.login.domain.Response loginResponse = new hello.login.domain.Response();
    loginResponse.setStatus("success");
    when(loginServiceClient.verify(TOKEN)).thenReturn(loginResponse); 

    //customer created successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerServiceClient.create(any(Customer.class))).thenReturn(response); 

    //customer to POST
    String json = mapper.writeValueAsString(customer);
  
    //POST to register a new customer
    mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).header("token", TOKEN).content(json))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.customer", notNullValue()))
      .andExpect(jsonPath("$.status", is("success")));
 
    verify(loginServiceClient, times(1)).verify(TOKEN);
    verify(customerServiceClient, times(1)).create(any(Customer.class));
  }

  @Test
  public void shouldNOTCreateNewCustomerWhenBadPhoneNumber() throws Exception {

    final String TOKEN = "12346uwfbdasBchSDBchjsDc.zjxczkjxczkjxcbjzxc.aiwhsjznckjznckjnzxcjk";
    final String PASSWORD = "pwd";
    final Customer customer = new Customer();
    final Integer PLATINUM_SALARY = 50001;
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setPassword(PASSWORD);
    customer.setSalary(PLATINUM_SALARY);
   
    //phone number is not provided
    //customer.setPhoneNumber(null);
    
    //token verified successfully
    hello.login.domain.Response loginResponse = new hello.login.domain.Response();
    loginResponse.setStatus("success");
    when(loginServiceClient.verify(TOKEN)).thenReturn(loginResponse);

    //customer created successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerServiceClient.create(any(Customer.class))).thenReturn(response); 

    //customer to POST
    String json = mapper.writeValueAsString(customer);

    mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON).header("token", TOKEN).content(json))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.statusText", is("Bad phone number")))
      .andExpect(jsonPath("$.customer", nullValue() ))
      .andExpect(jsonPath("$.status", is("fail")));
 
    //should not create BECAUSE of "bad phone number" error
    verify(loginServiceClient, times(1)).verify(TOKEN);
    verify(customerServiceClient, times(0)).create(any(Customer.class));
  }

  @Test
  public void shouldFindById() throws Exception {

    final String TOKEN = "12346uwfbdasBchSDBchjsDc.zjxczkjxczkjxcbjzxc.aiwhsjznckjznckjnzxcjk";
    final String ID = "123456";
    final String REF_CODE = "201801011234";
    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setReferenceCode(new ReferenceCode());
    customer.getReferenceCode().setData(REF_CODE);
    customer.setMemberType(Customer.MEMBER_TYPE_GOLD);
  
    //token verified successfully
    hello.login.domain.Response loginResponse = new hello.login.domain.Response();
    loginResponse.setStatus("success");
    when(loginServiceClient.verify(TOKEN)).thenReturn(loginResponse); 

    //customer found successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerServiceClient.findById(ID)).thenReturn(response); 

  
    //send GET to find customer by Id 
    final String URI = "/register/search?id=" + ID;
    mockMvc.perform(get(URI).header("token", TOKEN))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.customer", notNullValue()))
      .andExpect(jsonPath("$.status", is("success")))
      .andExpect(jsonPath("$.customer.firstName", is("foo")))
      .andExpect(jsonPath("$.customer.referenceCode.data", is(REF_CODE)))
      .andExpect(jsonPath("$.customer.memberType", is(Customer.MEMBER_TYPE_GOLD)));
 
    verify(loginServiceClient, times(1)).verify(TOKEN);
    verify(customerServiceClient, times(1)).findById(ID);
  }

  @Test
  public void shouldFindByFirstNameAndLastName() throws Exception {

    final String TOKEN = "12346uwfbdasBchSDBchjsDc.zjxczkjxczkjxcbjzxc.aiwhsjznckjznckjnzxcjk";
    final String FIRST_NAME = "foo";
    final String LAST_NAME = "bar";
    final String REF_CODE = "201801011234";
    final Customer customer = new Customer();
    final Integer PLATINUM_SALARY = 50001;
    customer.setFirstName(FIRST_NAME);
    customer.setLastName(LAST_NAME);
    customer.setReferenceCode(new ReferenceCode());
    customer.getReferenceCode().setData(REF_CODE);
    customer.setMemberType(Customer.MEMBER_TYPE_GOLD);
  
    //token verified successfully
    hello.login.domain.Response loginResponse = new hello.login.domain.Response();
    loginResponse.setStatus("success");
    when(loginServiceClient.verify(TOKEN)).thenReturn(loginResponse); 

    //customer found successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerServiceClient.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME)).thenReturn(response); 

    //send GET to find customer by FirstNameAndLastName 
    final String URI = "/register/search?firstName=" + FIRST_NAME + "&lastName=" + LAST_NAME;
    mockMvc.perform(get(URI).header("token", TOKEN))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.customer", notNullValue()))
      .andExpect(jsonPath("$.status", is("success")))
      .andExpect(jsonPath("$.customer.firstName", is(FIRST_NAME)))
      .andExpect(jsonPath("$.customer.referenceCode.data", is(REF_CODE)))
      .andExpect(jsonPath("$.customer.memberType", is(Customer.MEMBER_TYPE_GOLD)));
 
    verify(loginServiceClient, times(1)).verify(TOKEN);
    verify(customerServiceClient, times(1)).findByFirstNameAndLastName(FIRST_NAME, LAST_NAME);
  }
}
