package hello.customer.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import hello.Application;
import hello.customer.domain.Customer;
import hello.customer.service.CustomerService;
import hello.customer.service.CustomerServiceImpl;
import hello.customer.repository.CustomerRepository;
import hello.customer.domain.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class CustomerServiceTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @InjectMocks
  private CustomerService customerService = new CustomerServiceImpl();
  
  @Mock
  private CustomerRepository customerRepository;

  private MockMvc mockMvc;
  
  @Before
  public void setup() {
  	initMocks(this);
  	this.mockMvc = MockMvcBuilders.standaloneSetup(customerService).build();
  }

  @Test
  public void shouldCreateNewCustomer() throws Exception {

    final String PASSWORD = "pwd";
    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setPassword(PASSWORD);
  
    when(customerRepository.findByFirstNameAndLastName("foo", "bar")).thenReturn(null); //existing NOT found
    when(customerRepository.save(any(Customer.class))).thenReturn(null);
  
    Response response = customerService.create(customer);
    Assert.assertTrue(response.getStatus().equals("success"));

    //expect password to be encrypted, Hence, not the same.
    Assert.assertFalse(customer.getPassword().equals(PASSWORD));

    verify(customerRepository, times(1)).findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName());
    verify(customerRepository, times(1)).save(any(Customer.class));

  }


  @Test
  public void shouldNotCreateNewCustomerWhenAlreadyExists() throws Exception {

    //Try to create a new customer "foo bar" but already exists
    final String PASSWORD = "pwd";
    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setPassword(PASSWORD);
  
    when(customerRepository.findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName()))
      .thenReturn(customer); //existing "foo bar" found
  
    Response response = customerService.create(customer);
    Assert.assertTrue(response.getStatus().equals("fail"));

    verify(customerRepository, times(1)).findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName());
    verify(customerRepository, times(0)).save(any(Customer.class)); //save() not called 
  }

  @Test
  public void shouldReturnSuccessIfFindById() throws Exception {

    final String ID = "123456";
    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("bar");
    customer.setId(ID);

    when(customerRepository.findById(ID)).thenReturn(customer);

    Response response = customerService.findById(ID);
    Assert.assertTrue(response.getStatus().equals("success"));
    Assert.assertTrue(response.getCustomer() == customer);

    verify(customerRepository, times(1)).findById(ID);
  }

  @Test
  public void shouldReturnFailIfNotFindById() throws Exception {

    final String ID = "123456";

    when(customerRepository.findById(ID)).thenReturn(null); //not found in DB

    Response response = customerService.findById(ID);
    Assert.assertTrue(response.getStatus().equals("fail"));
    Assert.assertTrue(response.getCustomer() == null);

    verify(customerRepository, times(1)).findById(ID);
  }

  @Test
  public void shouldReturnSuccessIfFindByFirstNameAndLastName() throws Exception {

    final String ID = "123456";
    final String FIRST_NAME = "foo";
    final String LAST_NAME = "bar";
    final Customer customer = new Customer();
    customer.setFirstName(FIRST_NAME);
    customer.setLastName(LAST_NAME);
    customer.setId(ID);

    when(customerRepository.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME)).thenReturn(customer);

    Response response = customerService.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME);
    Assert.assertTrue(response.getStatus().equals("success"));
    Assert.assertTrue(response.getCustomer() == customer);

    verify(customerRepository, times(1)).findByFirstNameAndLastName(FIRST_NAME, LAST_NAME);
  }

  @Test
  public void shouldReturnFailIfNotFindByFirstNameAndLastName() throws Exception {

    final String FIRST_NAME = "foo";
    final String LAST_NAME = "bar";

    when(customerRepository.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME)).thenReturn(null); //not found in DB

    Response response = customerService.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME);
    Assert.assertTrue(response.getStatus().equals("fail"));
    Assert.assertTrue(response.getCustomer() == null);

    verify(customerRepository, times(1)).findByFirstNameAndLastName(FIRST_NAME, LAST_NAME);
  }
}
