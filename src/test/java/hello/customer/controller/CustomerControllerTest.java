package hello.customer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import hello.Application;
import hello.customer.domain.Customer;//com.piggymetrics.auth.domain.User;
import hello.customer.service.CustomerService;
import hello.customer.domain.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.beans.factory.annotation.Autowired;

import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.hamcrest.Matchers.is;
import org.hamcrest.Matcher;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class CustomerControllerTest {

  private static final ObjectMapper mapper = new ObjectMapper();

  @InjectMocks
  private CustomerController customerController;
  
  @Mock
  private CustomerService customerService;

  private MockMvc mockMvc;
  
  @Before
  public void setup() {
  	initMocks(this);
  	this.mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
  }

  @Test
  public void shouldCreateNewCustomer() throws Exception {

    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("fooson");
    customer.setPassword("password");
  
    //Mock customer created successfully
    Response response = new Response();
    response.setStatus("success");
    when(customerService.create(any())).thenReturn(response);
  
    String json = "{ \"firstName\": \"foo\", \"lastName\": \"bar\", \"password\": \"pwd\"}";
  
    mockMvc.perform(post("/customers").contentType(MediaType.APPLICATION_JSON).content(json))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.status", is("success")));
  }


  @Test
  public void shouldCallFindById() throws Exception {

    final Customer customer = new Customer();
    customer.setFirstName("foo");
    customer.setLastName("bar");

    //Mock customer found successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerService.findById(any())).thenReturn(response);
  
    mockMvc.perform(get("/customers?id=123456"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.status", is("success")))
      .andExpect(jsonPath("$.customer.firstName", is("foo")))
      .andExpect(jsonPath("$.customer.lastName", is("bar")));
  }

  @Test
  public void shouldCallFindByFirstNameAndLastName() throws Exception {

    final String FIRST_NAME = "foo";
    final String LAST_NAME = "bar";

    Customer customer = new Customer();
    customer.setFirstName(FIRST_NAME);
    customer.setLastName(LAST_NAME);

    //Mock customer found successfully
    Response response = new Response();
    response.setStatus("success");
    response.setCustomer(customer);
    when(customerService.findByFirstNameAndLastName(FIRST_NAME, LAST_NAME)).thenReturn(response);
  
    final String URI = "/customers?firstName=" + FIRST_NAME + "&lastName=" + LAST_NAME;
    mockMvc.perform(get(URI))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.status", is("success")))
      .andExpect(jsonPath("$.customer.firstName", is(FIRST_NAME)))
      .andExpect(jsonPath("$.customer.lastName", is(LAST_NAME)));
  }
}


