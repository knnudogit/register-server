package hello.register.controller;

import hello.customer.domain.Customer;
import hello.customer.domain.Response;
import hello.customer.domain.ReferenceCode;
import hello.register.service.CustomerServiceClient;
import hello.login.service.LoginServiceClient;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/register")
public class RegisterController {

  private final Logger log = LoggerFactory.getLogger(getClass());
  private static final String template = "Hola, %s!";
  private static final String rejectTemplate = "Sorry, %s. Fail to meet minimum requirement";
  private final AtomicLong counter = new AtomicLong();

  @Autowired
  private CustomerServiceClient customerServiceClient;

  @Autowired
  private LoginServiceClient loginServiceClient;


  private boolean verify(String token) {

    hello.login.domain.Response loginResponse = loginServiceClient.verify(token);
    return loginResponse.getStatus().equals("success");
  }


  @GetMapping(path="/search", params = "id")
  public Response findById(
    @RequestHeader(required=true, name="token") String token,
    @RequestParam (value="id")                  String id) {
    
    if (!verify(token)) {
        return new Response("fail", "Bad token");
    }

    return customerServiceClient.findById(id);
  }


  @GetMapping(path="/search", params = {"firstName" , "lastName"})
  public Response findByFirstNameAndLastName(
    @RequestHeader(required=true, name="token") String token,
    @RequestParam(value="firstName")            String firstName,
    @RequestParam(value="lastName")             String lastName) {
    
    if (!verify(token)) {
        return new Response("fail", "Bad token");
    }

    return customerServiceClient.findByFirstNameAndLastName(firstName, lastName);
  }


  @PostMapping()
  public Response create(
    @RequestHeader(required=true, name="token") String token,
    @RequestBody  (required=true)               Customer customer) {

    if (!verify(token)) {
        return new Response("fail", "Bad token");
    }

    try {
      Assert.notNull(customer.getSalary(),      "invalid salary");
      Assert.notNull(customer.getFirstName(),   "invalid first name");
      Assert.notNull(customer.getLastName(),    "invalid last name");
      Assert.notNull(customer.getPassword(),    "invalid password");
      Assert.notNull(customer.getPhoneNumber(), "invalid phone number");
      Assert.notNull(customer.getAddress(),     "invalid address");

      //process the customer info
      int memberType = computeMemberType(customer.getSalary());

      if (memberType == Customer.MEMBER_TYPE_UNDEFINED) {
        //reject the application
        return new Response("fail", String.format(rejectTemplate, customer.getFirstName()));
      }

      customer.setMemberType(memberType);
  
      //create a reference code
      ReferenceCode refCode = new ReferenceCode(customer.getPhoneNumber());
      customer.setReferenceCode(refCode);

      //save to DB
      Response response = customerServiceClient.create(customer);
  
      return response;

    } catch (Exception e) {
      log.error("RegisterController:create(): exception: {}", e.getMessage()); 
      return new Response("fail", e.getMessage());
    }
  }


  //todo: public for unit test!
  public int computeMemberType(final Integer salary) {

      int memberType = Customer.MEMBER_TYPE_UNDEFINED;

      //process the customer info
      if (salary > 50000) {
        memberType = Customer.MEMBER_TYPE_PLATINUM;
      } else if (30000 <= salary && salary <= 50000) {
        memberType = Customer.MEMBER_TYPE_GOLD;
      } else if (15000 <= salary && salary < 30000) {
        memberType = Customer.MEMBER_TYPE_SILVER;
      } 

      return memberType;
  }
}
