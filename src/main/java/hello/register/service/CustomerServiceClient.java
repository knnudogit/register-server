package hello.register.service;

import hello.customer.domain.Customer;
import hello.customer.domain.Response;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Interface to the CustomerService
 * - create a new customer
 * - look up a customer by full name
 * - look up a customer by ID
 */

@Service
public class CustomerServiceClient {

  private static final Logger log = LoggerFactory.getLogger(CustomerServiceClient.class);

  public Response findById(String id) {

    RestTemplate restTemplate = new RestTemplate();
    Response response = restTemplate.getForObject(
      "http://localhost:8080/customers?" +
      "id=" + id,
      Response.class
    ); 
    log.debug("{}", response);

    return response;
  }

  public Response findByFirstNameAndLastName(String firstName, String lastName) {

    RestTemplate restTemplate = new RestTemplate();
    Response response = restTemplate.getForObject(
      "http://localhost:8080/customers?" +
      "firstName=" + firstName +
      "&lastName=" + lastName,
      Response.class
    ); 
    log.debug("{}", response);

    return response;
  }

  public Response create(Customer customer) {

    RestTemplate restTemplate = new RestTemplate();
    Response response = restTemplate.postForObject(
      "http://localhost:8080/customers",
      customer,
      Response.class
    ); 
    log.debug("{}", response);

    return response;
  }
}




