package hello.customer.service;

import hello.customer.domain.Customer;
import hello.customer.domain.Response;
import hello.customer.repository.CustomerRepository;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class CustomerServiceImpl implements CustomerService {

  private final Logger log = LoggerFactory.getLogger(getClass());
  private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


  @Autowired
  private CustomerRepository repository;


  @Override
  public Response create(Customer customer) {

    //check if already exists 
    Customer existing = repository.findByFirstNameAndLastName(customer.getFirstName(), customer.getLastName()); 

    //if exists, return error
    if (existing != null) {
      final String message = "Customer already exists: " + existing.getFullName() + ", " + existing.getId();
      log.error("Error: " + message);
      return new Response("fail", message);
    }
    
    //encrypt password
    String hash = encoder.encode(customer.getPassword());
    customer.setPassword(hash);

    //add to DB
    repository.save(customer);

    log.debug("Success: Customer created: {}", customer.getFullName());
    return new Response("success", "Customer created", customer);
  }


  @Override
  public Response findById(String customerId) {

    Customer customer = repository.findById(customerId); 
    return (customer == null)?
      new Response("fail", "Customer not found"):
      new Response("success", "", customer);
  }


  @Override
  public Response findByFirstNameAndLastName(String firstName, String lastName) {

    Customer customer = repository.findByFirstNameAndLastName(firstName, lastName); 
    return (customer == null)?
      new Response("fail", "Customer not found"):
      new Response("success", "", customer);
  }
}
