package hello.customer.service;

import hello.customer.domain.Response;
import hello.customer.domain.Customer;

/**
 * A registered customer related service.
 * - Create a new customer in the data store.
 * - Search for an existng customer in the data store.
 *
 * Used primarily by CustomerController.
 * The datastore is in CustomerRepository.
 **/
public interface CustomerService {

  Response create                    (Customer customer);
  Response findById                  (String customerId);
  Response findByFirstNameAndLastName(String firstName, String lastName);

}
