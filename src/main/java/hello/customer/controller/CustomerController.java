package hello.customer.controller;

import hello.customer.domain.Response;
import hello.customer.domain.Customer;
import hello.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/customers")
public class CustomerController {

  @Autowired
  private CustomerService customerService;


  @RequestMapping(method = RequestMethod.GET, params = "id")
  public Response getUser(
      @RequestParam(value="id", defaultValue="0")
      String customerId) {

    return customerService.findById(customerId);
  }


  @RequestMapping(method = RequestMethod.GET, params = {"firstName", "lastName"})
  public Response getUserByName(
      @RequestParam(value="firstName", defaultValue="")
      String firstName,
      @RequestParam(value="lastName", defaultValue="")
      String lastName) {

    return customerService.findByFirstNameAndLastName(firstName, lastName);
  }


  @RequestMapping(method = RequestMethod.POST)
  public Response createUser(
      @Valid @RequestBody Customer customer) {

    return customerService.create(customer);
  }
}


