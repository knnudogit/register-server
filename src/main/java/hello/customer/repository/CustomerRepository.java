package hello.customer.repository;

import hello.customer.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

/** 
 * Data store for registered customers 
 **/
@Repository
public interface CustomerRepository extends CrudRepository<Customer, String> {

  List<Customer> findByLastName            (String lastName);
  Customer       findByFirstNameAndLastName(String firstName, String lastName); //firstname + lastName is unique
  Customer       findById                  (String id);

} 
