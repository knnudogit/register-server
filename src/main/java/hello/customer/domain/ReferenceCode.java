package hello.customer.domain;

import java.lang.Exception;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.FieldPosition;

//
// The reference code format:
// date (YYYYMMDD) + last 4 digits of customer phone number (at registration time)
// Example: "201802140998"
//
public class ReferenceCode {
  String data;

  public ReferenceCode() {
    setData("");
  }

  public ReferenceCode(String phoneNumber) throws Exception {

   //validate phone number
   if (phoneNumber == null || phoneNumber.length() < 4) {
     throw new Exception("Bad phone number");
   }

   //get current date string
   SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
   StringBuffer buf = new StringBuffer();
   FieldPosition fp = new FieldPosition(0);
   df.format(new Date(), buf, fp);

   setData(buf.toString() + 
           phoneNumber.substring(phoneNumber.length() - 4));
   
  }

  public String getData() {
    return data;
  }

  public void setData(String data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return data;
  }
}

