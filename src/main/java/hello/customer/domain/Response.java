package hello.customer.domain;

import hello.customer.domain.Customer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Response from /customers URI
 * is in json format
 * status: "fail" or "success"
 * statusText: <status details text>
 * customer: <Customer json> or null
 **/
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
  private String status;
  private String statusText;
  private Customer customer;

  public Response() {
  }

  public Response(String status, String statusText) {
    setStatus(status);
    setStatusText(statusText);
    setCustomer(null);
  }

  public Response(String status, String statusText, Customer customer) {
    setStatus(status);
    setStatusText(statusText);
    setCustomer(customer);
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatusText() {
    return statusText;
  }

  public void setStatusText(String statusText) {
    this.statusText = statusText;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  @Override
  public String toString() {
    String customerString =  (customer == null)? "NULL": customer.toString();
    return "Response{" +
      "status=" + getStatus() + ", " +
      "statusText=" + getStatusText() + ", " +
      "customer=" + customerString +
    "}";
  }
}
