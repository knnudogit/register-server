package hello.customer.domain;

import hello.customer.domain.ReferenceCode;
import org.springframework.data.annotation.Id;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

  public static final int MEMBER_TYPE_UNDEFINED = 0;
  public static final int MEMBER_TYPE_PLATINUM = 1;
  public static final int MEMBER_TYPE_GOLD = 2;
  public static final int MEMBER_TYPE_SILVER = 3;
  public static final String[] MEMBER_TYPE_NAME = {"Undefined", "Platinum", "Gold", "Silver"};

  public static final int MEMBER_TYPE_MIN = MEMBER_TYPE_UNDEFINED;
  public static final int MEMBER_TYPE_MAX = MEMBER_TYPE_SILVER;

  @Id private String     id;
  private String         firstName;
  private String         lastName;
  private String         password;
  private String         phoneNumber;
  private String         address;
  private Integer        salary;
  private int            memberType;
  private ReferenceCode  referenceCode;
  
  public Customer() {
    setMemberType(MEMBER_TYPE_UNDEFINED);
    setFirstName("");
    setLastName("");
    setPhoneNumber("");
    setAddress("");
    setSalary(0);
    setReferenceCode(new ReferenceCode());
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Integer getSalary() {
    return salary;
  }

  public void setSalary(Integer salary) {
    this.salary = salary;
  }

  public ReferenceCode getReferenceCode() {
    return referenceCode;
  }

  public void setReferenceCode(ReferenceCode referenceCode) {
    this.referenceCode = referenceCode;
  }

  public int getMemberType() {
    return memberType;
  }

  public void setMemberType(int memberType) {
    if( memberType < MEMBER_TYPE_MIN || 
        MEMBER_TYPE_MAX < memberType) {
      return;
    }  
    this.memberType = memberType;
  }

  public String getMemberTypeString() {
    if( getMemberType() < MEMBER_TYPE_MIN || 
        MEMBER_TYPE_MAX < getMemberType()) {
      return MEMBER_TYPE_NAME[MEMBER_TYPE_UNDEFINED];
    }  
    return MEMBER_TYPE_NAME[getMemberType()];
  }

  public String getFullName() {
    return getFirstName() + " " + getLastName();
  }

  @Override
  public String toString() {
    return "Customer{" +
      "firstName=" + getFirstName() + ", " +
      "lastName=" + getLastName() + ", " +
      "password=" + getPassword() + ", " +
      "phoneNumber=" + getPhoneNumber() + ", " +
      "address=" + getAddress() + ", " +
      "salary=" + getSalary() + ", " +
      "referenceCode=" + getReferenceCode() + ", " +
      "memberType=" + getMemberTypeString() +
    "}";
  }
}
