package hello.login.repository;

import org.springframework.stereotype.Repository;
import hello.login.domain.User;
import java.util.Vector;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * This class is FOR DEMO PURPOSES ONLY
 * it mocks a DB storing user information.
 **/
@Repository
public class UserRepository {

  private Vector<User> users = new Vector();
  private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();


  public UserRepository() {
    users.add(new User("admin1", encoder.encode("password"), User.ROLE_ADMIN));
    users.add(new User("admin2", encoder.encode("password"), User.ROLE_ADMIN));
    users.add(new User("bob",    encoder.encode("password"), User.ROLE_GENERAL_USER));
    users.add(new User("ben",    encoder.encode("password"), User.ROLE_GENERAL_USER));
  }


  public User findByName(String name) {

    for (int i=0; i<users.size(); ++i) {
      User u = users.get(i);
      if (u.getName().equals(name)) {
        return u;
      }
    }
    return null;
  }
}


