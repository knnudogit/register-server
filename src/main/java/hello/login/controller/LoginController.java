package hello.login.controller;

import hello.login.domain.Response;
import hello.login.domain.User;
import hello.login.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/login")
public class LoginController {

  @Autowired
  private LoginService loginService;

  @RequestMapping(method = RequestMethod.POST)
  public Response login(@Valid @RequestBody User user) {
    return loginService.login(user);
  }

  @GetMapping(path="/verify", params = "token")
  public Response verify(@RequestParam(value="token") String token) {

    if(loginService.verify(token)) {
      return new Response("success", "");
    }
    return new Response("fail", "Token verification failed");
  }
}


