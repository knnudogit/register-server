package hello.login.service;

import hello.login.domain.User;
import hello.login.domain.Response;
import hello.login.repository.UserRepository;
import com.auth0.jwt.algorithms.Algorithm; 
import com.auth0.jwt.JWT; 
import com.auth0.jwt.exceptions.JWTCreationException; 
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class LoginServiceImpl implements LoginService {

  private static final Logger log = LoggerFactory.getLogger(LoginServiceImpl.class);
  private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
  private static final Algorithm algorithm = createJWTAlgorithm();
  private static final JWTVerifier verifier = createJWTVerifier();

  @Autowired
  private UserRepository repository;


  private static Algorithm createJWTAlgorithm() {
    try {
      return Algorithm.HMAC256("mysecret");

    } catch (java.io.UnsupportedEncodingException e) {
      log.error("LoginService: {}", e.getMessage());
      return null;
    } 
  }


  private static JWTVerifier createJWTVerifier() {
      return JWT.require(algorithm).build(); //create a verifier
  }


  @Override
  public Response login(User user) {

    //validate params
    try {
      Assert.notNull(user.getName(), "No user name");
      Assert.notNull(user.getPassword(), "No password");

    } catch (IllegalArgumentException e) {
      log.error("LoginService: validateParams(): {}", e.getMessage());
      return new Response("fail", e.getMessage());
    }

    //look up user
    User existing = repository.findByName(user.getName()); 
    if (existing == null) {
      log.error("LoginService: invalid user: {}", user.getName());
      return new Response("fail", "Invalid user");
    }

    //verify password 
    if (encoder.matches(user.getPassword(), existing.getPassword()) == false) {
      log.error("LoginService: invalid password");
      return new Response("fail", "Invalid password");
    }
    
    //create token ONLY IF user is admin
    //String token = "";
    //if (existing.getRole() == User.ROLE_ADMIN) {
    //  token = createToken(existing);
    //  if (token == null) {
    //    return new Response("fail", "Cannot create token");
    //  }
    //}

    //create token accroding to user's role
    String token = createToken(existing);
    if (token == null) {
      return new Response("fail", "Cannot create token");
    }

    log.debug("Success: user logged in: {}", user.getName());
    return new Response("success", "", token);
  }


  //create JWT token
  private String createToken(User existing) {
    try {
      String token = JWT.create()
        .withClaim("name", existing.getName())
        .withClaim("role", existing.getRole())
        .sign(algorithm);
  
      log.debug("JWT created for: {}, {}", existing.getName(), existing.getRole());
      log.debug("JWT created: {}", token);
  
      return token;

    } catch (JWTCreationException  e) {
      log.error("LoginService: {}", e.getMessage());
      return null;
    } 
  }


  @Override
  public boolean verify(String token) {
    
    DecodedJWT jwt = decode(token);
    if (jwt == null) {
      return false;
    }

    if (!isAdmin(jwt)) {
      log.error("LoginService: Role verify failed");
      return false;
    }
      
    log.debug("Success: token verified: user: {}", jwt.getClaim("name").asString());

    return true;
  }


  //decode JWT
  private DecodedJWT decode(String token) {

    log.debug("decode token: {}", token);

    try {
      DecodedJWT jwt = verifier.verify(token);
      if (jwt == null || jwt.getClaim("name").isNull() || jwt.getClaim("role").isNull()) {
        log.error("LoginService: Claim not found in token");
        return null;
      }

      log.debug("name claim is {}", jwt.getClaim("name").asString());
      log.debug("role claim is {}", jwt.getClaim("role").asString());
      return jwt;

    } catch ( JWTCreationException | SignatureVerificationException e) {
      log.error("LoginService: decode(): {}", e.getMessage());
      return null;
    }
  }


  //return true if token contains "admin" role
  private boolean isAdmin(DecodedJWT jwt) {
    return jwt.getClaim("role").asString().equals(User.ROLE_ADMIN);
  }

}
