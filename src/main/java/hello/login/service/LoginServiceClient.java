package hello.login.service;

import hello.login.domain.User;
import hello.login.domain.Response;
import org.springframework.web.client.RestTemplate;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * Interface to the LoginService
 * - verify a jwt 
 */

@Service
public class LoginServiceClient {

  private static final Logger log = LoggerFactory.getLogger(LoginServiceClient.class);

  public Response verify(String token) {

    RestTemplate restTemplate = new RestTemplate();
    Response response = restTemplate.getForObject(
      "http://localhost:8080/login/verify?" +
      "token=" + token,
      Response.class
    ); 
    log.debug("{}", response);

    return response;
  }
}
