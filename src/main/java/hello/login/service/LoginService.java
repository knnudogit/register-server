package hello.login.service;

import hello.login.domain.Response;
import hello.login.domain.User;

public interface LoginService {

  Response login  (User user);
  boolean  verify (String token);

}
