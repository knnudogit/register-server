package hello.login.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
  private String status;
  private String statusText;
  private String token;

  public Response() {
  }

  public Response(String status, String statusText) {
    setStatus(status);
    setStatusText(statusText);
    setToken("");
  }

  public Response(String status, String statusText, String token) {
    setStatus(status);
    setStatusText(statusText);
    setToken(token);
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatusText() {
    return statusText;
  }

  public void setStatusText(String statusText) {
    this.statusText = statusText;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public String toString() {
    return "Response{" +
      "status=" + getStatus() + ", " +
      "statusText=" + getStatusText() + ", " +
      "token=" + getToken() +
    "}";
  }
}
