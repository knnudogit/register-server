package hello.login.domain;

public class User {
  public  static final String ROLE_ADMIN = "admin";
  public  static final String ROLE_GENERAL_USER = "guser";
  private String name;
  private String password;
  private String role;

  public User() {
  }

  public User(String name, String password, String role) {
    setName(name);
    setPassword(password);
    setRole(role);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public String toString() {
    return "User{" +
      "name=" + getName() + ", " +
      "password=" + getPassword() + ", " +
      "role=" + getRole() +
      "}";
 }
}
